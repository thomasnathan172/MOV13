from fastapi import FastAPI, Depends
from authenticator import authenticator
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import accounts
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from typing import Annotated


app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)

security = HTTPBasic()


@app.get("/users/me")
def read_current_user(credentials: HTTPBasicCredentials = Depends(security)):
    return {"username": credentials.username, "password": credentials.password}






# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=[
#         os.environ.get("CORS_HOST")
#     ],
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )


# @app.get("/api/launch-details")
# def launch_details():
#     return {
#         "launch_details": {
#             "module": 3,
#             "week": 17,
#             "day": 5,
#             "hour": 19,
#             "min": "00"
#         }
#     }
