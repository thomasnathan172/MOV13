from pydantic import BaseModel
from typing import Optional, List




class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str
    full_name: str
    email: str






class AccountOut(BaseModel):
    email: str
    full_name: str

class AccountOutWithPassword(AccountOut):
    hashed_password: str

class AccountQueries:
    def __init__(self):
        pass  # You can add initialization logic here if needed

    def get_account_by_email(self, email: str) -> Optional[AccountOutWithPassword]:
        # Logic to retrieve an account from the database or storage by email
        # This function should return an AccountOutWithPassword object or None if not found
        pass

    def create_account(self, info: AccountIn, hashed_password: str) -> AccountOutWithPassword:
        # Check if an account with the given email already exists
        existing_account = self.get_account_by_email(info.username)
        if existing_account:
            raise DuplicateAccountError("An account with this username already exists")

        # Logic to create a new account in the database or storage
        # This function should return an AccountOutWithPassword object
        pass

    def get_all_accounts(self) -> List[AccountOut]:
        # Logic to retrieve all accounts from the database or storage
        # This function should return a list of AccountOut objects
        pass



# import os
# from psycopg_pool import ConnectionPool
# pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))
