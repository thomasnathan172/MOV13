from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AccountQueries, AccountOutWithPassword, AccountOut
from dotenv import load_dotenv
import os
# Load environment variables from .env file
load_dotenv()

# Access the SIGNING_KEY environment variable
SIGNING_KEY = os.environ.get("SIGNING_KEY")

# Define secrets and algorithm
secrets = "17b/HeBMzCYt9EqCaWFR4NfKZ2GdoNxdU54"
algorithm = "HS256"

# Check if SIGNING_KEY is defined
if not SIGNING_KEY:
    # If SIGNING_KEY is not defined, use the default secrets and algorithm
    SIGNING_KEY = secrets

class ExampleAuthenticator(Authenticator):
    async def get_account_data(
        self,
        email: str,
        accounts: AccountQueries,
    ) -> AccountOutWithPassword:
        if accounts is None:
            return None
      
        return accounts.get_account_by_email(email)


    def get_account_getter(
        self,
        accounts: AccountQueries = Depends(),
    ) -> AccountQueries:
        # Return the accounts. That's it.
        return accounts.get_all_accounts()

    def get_hashed_password(self, account: AccountOutWithPassword) -> str:
        # Return the encrypted password value from your
        # account object
        return account.hashed_password

    def get_account_data_for_cookie(self, account: AccountOut):
        # Return the username and the data for the cookie.
        # You must return TWO values from this method.
        return account.username, AccountOut(**account.dict())

# Create an instance of ExampleAuthenticator with SIGNING_KEY
authenticator = ExampleAuthenticator(SIGNING_KEY)
